# Copyright 2020 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# CC Library : aead
source_set("aead") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "aead.h" ]
  public_deps = [
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : hybrid_decrypt
source_set("hybrid_decrypt") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "hybrid_decrypt.h" ]
  public_deps = [
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : hybrid_encrypt
source_set("hybrid_encrypt") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "hybrid_encrypt.h" ]
  public_deps = [
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : mac
source_set("mac") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "mac.h" ]
  public_deps = [
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : keyset_reader
source_set("keyset_reader") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "keyset_reader.h" ]
  public_deps = [
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : keyset_writer
source_set("keyset_writer") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "keyset_writer.h" ]
  public_deps = [
    "//third_party/tink/cc/util:status",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : binary_keyset_reader
source_set("binary_keyset_reader") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "binary_keyset_reader.h",
    "core/binary_keyset_reader.cc",
  ]
  public_deps = [
    ":keyset_reader",
    "//third_party/abseil-cpp/absl/memory:memory",
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/protobuf:protobuf_lite",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : binary_keyset_writer
source_set("binary_keyset_writer") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "binary_keyset_writer.h",
    "core/binary_keyset_writer.cc",
  ]
  public_deps = [
    ":keyset_writer",
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:protobuf_helper",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  configs += [ "//build/config:Wno-extra-semi" ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : catalogue
source_set("catalogue") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "catalogue.h" ]
  public_deps = [
    ":key_manager",
    "//third_party/abseil-cpp/absl/base:core_headers",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : crypto_format
source_set("crypto_format") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "core/crypto_format.cc",
    "crypto_format.h",
  ]
  public_deps = [
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : primitive_set
source_set("primitive_set") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "primitive_set.h",
    "primitive_set.h",
  ]
  public_deps = [
    ":crypto_format",
    "//third_party/abseil-cpp/absl/memory:memory",
    "//third_party/abseil-cpp/absl/synchronization:synchronization",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : primitive_wrapper
source_set("primitive_wrapper") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "primitive_wrapper.h" ]
  public_deps = [
    ":primitive_set",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : registry
source_set("registry") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "registry.h" ]
  public_deps = [
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/core:registry_impl",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : keyset_handle
source_set("keyset_handle") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "core/keyset_handle.cc",
    "keyset_handle.h",
  ]
  public_deps = [
    ":aead",
    ":key_manager",
    ":keyset_reader",
    ":keyset_writer",
    ":primitive_set",
    ":registry",
    "//third_party/abseil-cpp/absl/memory:memory",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : cleartext_keyset_handle
source_set("cleartext_keyset_handle") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "cleartext_keyset_handle.h",
    "core/cleartext_keyset_handle.cc",
  ]
  public_deps = [
    ":keyset_handle",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : key_manager
source_set("key_manager") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "core/key_manager.cc",
    "key_manager.h",
  ]
  public_deps = [
    "//third_party/abseil-cpp/absl/memory:memory",
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:protobuf_helper",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
    "//third_party/tink/proto:tink_proto",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : kms_client
source_set("kms_client") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [ "kms_client.h" ]
  public_deps = [
    ":aead",
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}

# CC Library : kms_clients
source_set("kms_clients") {
  configs += [ "//build/config:no_rtti" ]
  configs -= [ "//build/config:no_rtti" ]
  sources = [
    "core/kms_clients.cc",
    "kms_clients.h",
  ]
  public_deps = [
    ":kms_client",
    "//third_party/abseil-cpp/absl/base:base",
    "//third_party/abseil-cpp/absl/strings:strings",
    "//third_party/abseil-cpp/absl/synchronization:synchronization",
    "//third_party/tink/cc/util:errors",
    "//third_party/tink/cc/util:status",
    "//third_party/tink/cc/util:statusor",
  ]
  public_configs = [ "//third_party/tink:tink_config" ]
}
